const { ccclass, property } = cc._decorator;
import Player from "./player"

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Vec2)
    private linear_velocity: cc.Vec2 = new cc.Vec2(100, 100);

    @property(Player)
    player: Player = null;

    onCollide: boolean = false;
    state: number = 0;
    //speed: number = 100;
    type: string = "";
    speed: cc.Vec2;
    health: number = 100;
    anim = null;
    animState = null;
    isDead: boolean = false;
    isAattack01Completed: boolean = false;
    combo: number = 0;
    standAnim = null;
    standAnimState = null;
    blocked = false;
    dir: number = 1;
    private object_picked = null;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.node.name = "goblin";
        this.node.group = "enemy";
        this.type = "enemy";
        this.anim = this.getComponent(cc.Animation);
    }

    onBeginContact(contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        } else if (other.node.group == "object") {
            other.node.group = "player"
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);
        }
    }

    onEndContact(contact, self, other) {
        this.blocked = false;
    }

    update(dt) {
        this.playerMovement(dt);
        this.playerAnimation();
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
    }

    playerMovement(dt) {
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
        if(this.node.x - this.player.node.x < 300 && this.node.x - this.player.node.x > 50){
            this.speed.x = -25;
            this.node.scaleX = 1;
        }
        else if(this.player.node.x - this.node.x < 300 && this.player.node.x - this.node.x > 50){
            this.speed.x = 25;
            this.node.scaleX = -1;
        }
        else if(this.player.node.x - this.node.x > 300){
            
        }
        else if(this.node.x - this.player.node.x > 300){
            
        }
        if(this.node.y - this.player.node.y < 300 && this.node.y - this.player.node.y > 50){
            this.speed.y = -25;
        }
        else if(this.player.node.y - this.node.y < 300 && this.player.node.y - this.node.y > 50){
            this.speed.y = 25;
        }
        else if(this.player.node.y - this.node.y > 300){
            
        }
        else if(this.node.y - this.player.node.y > 300){
            
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = this.speed;
        console.log(this.player.node.x);
        console.log(this.player.node.y);
        console.log(this.node.x);
        console.log(this.node.y);
    }

    private playerAnimation()
    {
        if (this.isDead) {
            this.animState = this.anim.play("death");
        }
        if(this.player.node.x - this.node.x < 50 && this.player.node.x - this.node.x > 0 && ((this.player.node.y - this.node.y < 50 && this.player.node.y - this.node.y > 0)||this.node.y - this.player.node.y < 50 && this.node.y - this.player.node.y > 0)){
            if(!this.anim.getAnimationState("attack").isPlaying){
                this.animState = this.anim.play("attack");
            }
        }
        else if(this.node.x - this.player.node.x < 50 && this.node.x - this.player.node.x > 0  && ((this.player.node.y - this.node.y < 50 && this.player.node.y - this.node.y > 0)||this.node.y - this.player.node.y < 50 && this.node.y - this.player.node.y > 0)){
            if(!this.anim.getAnimationState("attack").isPlaying){
                this.animState = this.anim.play("attack");
            }
        }
        else if(this.node.x - this.player.node.x < 200 && this.node.x - this.player.node.x > 50){
            if(!this.anim.getAnimationState("move").isPlaying){
                this.animState = this.anim.play("move");
            }
        }
        else if(this.player.node.x - this.node.x < 200 && this.player.node.x - this.node.x > 50){
            if(!this.anim.getAnimationState("move").isPlaying){
                this.animState = this.anim.play("move");
            }
        }
        else if(this.player.node.x - this.node.x > 200){
            if(!this.anim.getAnimationState("stand").isPlaying){
                this.animState = this.anim.play("stand");
            }
        }
        else if(this.node.x - this.player.node.x > 200){
            if(!this.anim.getAnimationState("stand").isPlaying){
                this.animState = this.anim.play("stand");
            }
        }
        if(this.node.y - this.player.node.y < 200 && this.node.y - this.player.node.y > 50){
            if(!this.anim.getAnimationState("move").isPlaying){
                this.animState = this.anim.play("move");
            }
        }
        else if(this.player.node.y - this.node.y < 200 && this.player.node.y - this.node.y > 50){
            if(!this.anim.getAnimationState("move").isPlaying){
                this.animState = this.anim.play("move");
            }
        }
        else if(this.player.node.y - this.node.y > 200){
            if(!this.anim.getAnimationState("stand").isPlaying){
                this.animState = this.anim.play("stand");
            }
        }
        else if(this.node.y - this.player.node.y > 200){
            if(!this.anim.getAnimationState("stand").isPlaying){
                this.animState = this.anim.play("stand");
            }
        }
    }
}
