// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import player from "./player";
import Player_ryu from "./player_ryu";
import Player from "./player_rockman";
@ccclass
export default class game_manager extends cc.Component {

    @property(player)
    player: player = null;
    player_ryu: Player_ryu = null;
    Player:Player = null;

    @property(cc.Camera)
    camera: cc.Camera = null;

    @property(cc.Canvas)
    canvas: cc.Canvas = null;

    @property(cc.TiledMap)
    private tilemap: cc.TiledMap = null;
    camera_can_move_width = true;
    camera_can_move_height = true;
    canvasWidth: number = 0;
    canvasHeight: number = 0;
    mapWidth: number = 0;
    mapHeight: number = 0;
    isDioUsingUlt: boolean = false;


    onLoad() {
        let p = cc.director.getPhysicsManager();
        p.enabled = true;
        p.gravity = new cc.Vec2(0, 0);
        p.debugDrawFlags = 1;
    }

    start() {
        this.canvasWidth = this.canvas.node.width;
        this.canvasHeight = this.canvas.node.height;
        this.mapWidth = this.tilemap.node.width;
        this.mapHeight = this.tilemap.node.height;
        //var tilemap = this.node.getComponent(cc.TiledMap);
        var layer = this.tilemap.getLayer("barrier");
        var tilesize = this.tilemap.getTileSize();
        var layersize = layer.getLayerSize();

        for (var i = 0; i < layersize.width; i++) {
            for (var j = 0; j < layersize.height; j++) {
                var tile = layer.getTiledTileAt(i, j, true);
                if (tile.gid != 0) {
                    let body = tile.node.addComponent(cc.RigidBody);
                    body.type = cc.RigidBodyType.Static;
                    tile.node.group = "barrier"
                    let collider = tile.node.addComponent(cc.PhysicsBoxCollider);
                    collider.offset = cc.v2(tilesize.width / 2, tilesize.height / 2);
                    collider.size = tilesize;
                    collider.tag = 1;
                    collider.apply();
                }
            }
        }
    }

    update(dt) {
        if (this.player.state == this.player.stateList.usingUltimate == false) {
            if (this.player.node.x >= 0 + this.mapWidth / 2 - this.canvasWidth / 2 || this.player.node.x <= 0 - this.mapWidth / 2 + this.canvasWidth / 2) this.camera_can_move_width = false;
            else {
                this.camera_can_move_width = true;
            }
            if (this.player.node.y >= 0 + this.mapHeight / 2 - this.canvasHeight / 2 || this.player.node.y <= 0 - this.mapHeight / 2 + this.canvasHeight / 2) this.camera_can_move_height = false;
            else {
                this.camera_can_move_height = true;
            }

            if (this.camera_can_move_width && !this.player.blocked) this.camera.node.x = this.player.node.x;
            if (this.camera_can_move_height && !this.player.blocked) this.camera.node.y = this.player.node.y;
        }
    }
}
