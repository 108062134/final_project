// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;


    private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerxSpeed: number = 0;

    private playerySpeed: number = 0;

    private zDown: boolean = false; // key for player to go left

    private xDown: boolean = false; // key for player to go right

    private jDown: boolean = false; // key for player to shoot

    private kDown: boolean = false; // key for player to jump

    private cDown:boolean = false;

    private vDown:boolean = false;

    health: number = 100;

    blocked = false;

    private object_picked = null;

    


    private isDead: boolean = true;

    onLoad()
    {
        
        this.anim=this.getComponent(cc.Animation);
        cc.director.getCollisionManager().enabled = true;

        cc.director.getPhysicsManager().enabled = true;

        this.bulletPool = new cc.NodePool('Bullet');

        let maxBulletNum = 5;

        for(let i: number = 0; i < maxBulletNum; i++)
        {
            let bullet = cc.instantiate(this.bulletPrefab);

            this.bulletPool.put(bullet);

        }
    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        
    }

    update(dt)
    {
        this.playerMovement(dt);

        this.playerAnimation();
    }

    onBeginContact(contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        } else if (other.node.group == "object") {
            other.node.group = "player"
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);

        }
    }
    onEndContact(contact, self, other) {
        this.blocked = false;
    }


    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = true;

                this.xDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = true;

                this.zDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = true;

                break;

            case cc.macro.KEY.k:

                this.kDown = true;

                break;
            case cc.macro.KEY.c:

                this.cDown = true;

                this.vDown = false;

                break;
            case cc.macro.KEY.v:
                
                this.vDown = true;

                this.cDown = false;

                break;
            
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.z:

                this.zDown = false;

                break;

            case cc.macro.KEY.x:

                this.xDown = false;

                break;

            case cc.macro.KEY.j:

                this.jDown = false;

                break;

            case cc.macro.KEY.k:

                this.kDown = false;

                break;
            case cc.macro.KEY.c:

                this.cDown = false;

                break;
            case cc.macro.KEY.v:
                
                this.vDown = false;

                break;
            
        }
    }

    

    private playerMovement(dt)
    {
        if(this.isDead){
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if(this.jDown || this.anim.getAnimationState('shoot').isPlaying){
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if(this.zDown)
            this.playerxSpeed = -300;
        else if(this.xDown)
            this.playerxSpeed = 300;
        else if(this.cDown)
            this.playerySpeed = 300;
        else if(this.vDown)
            this,this.playerySpeed = -300;
        else{
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        this.node.x += this.playerxSpeed * dt;  //move player
        this.node.y += this.playerySpeed * dt;
    }

    private playerAnimation()
    {
        this.node.scaleX = (this.zDown) ? -1 : (this.xDown) ? 1 : this.node.scaleX;

        if(this.isDead)
        {
            //reset player position and play reborn animation
            if(this.animateState == null || this.animateState.name != 'reborn')
            {
                this.node.setPosition(cc.v2(102,-16));
                this.animateState = this.anim.play('reborn');
                this.anim.once("finished",() =>{
                    this.isDead = false;
                })
                
            }
        }
        else if(!this.anim.getAnimationState('shoot').isPlaying && !this.anim.getAnimationState('jump').isPlaying) // move animation can play only when shoot or jump animation finished
        {
            if(this.jDown)
                this.animateState = this.anim.play('shoot');
            else if(this.kDown)
            {
                    this.animateState = this.anim.play('jump');

            }
            else if(this.zDown || this.xDown || this.cDown || this.vDown)
            {
                if(this.animateState == null || this.animateState.name != 'move') // when first call or last animation is not move
                    this.animateState = this.anim.play('move');
            }
            else
            {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if(this.animateState == null || this.animateState.name != 'idle')
                    this.animateState = this.anim.play('idle');
            }
        }
    }

    //give velocity to the player

    // call this when player shoots the bullet.
    private createBullet()
    {
        let bullet = null;

        if (this.bulletPool.size() > 0) 
            bullet = this.bulletPool.get(this.bulletPool);

        if(bullet != null)
            bullet.getComponent('Bullet').init(this.node);
        cc.log("bullet rock");
    }

    //check if the collision is valid or not
}
