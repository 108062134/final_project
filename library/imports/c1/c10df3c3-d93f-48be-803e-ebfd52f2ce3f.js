"use strict";
cc._RF.push(module, 'c10dfPD2T9IvoA+6/1S8s4/', 'entry');
// scripts/entry.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var entry = /** @class */ (function (_super) {
    __extends(entry, _super);
    function entry() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    entry.prototype.onLoad = function () {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onkeyDown, this);
    };
    entry.prototype.onkeyDown = function (event) {
        cc.director.loadScene("login");
    };
    entry.prototype.start = function () {
    };
    entry = __decorate([
        ccclass
    ], entry);
    return entry;
}(cc.Component));
exports.default = entry;

cc._RF.pop();