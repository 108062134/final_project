"use strict";
cc._RF.push(module, 'fa391W65phObqD1UfKl9qoG', 'map');
// scripts/map.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var map = /** @class */ (function (_super) {
    __extends(map, _super);
    function map() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    map.prototype.onLoad = function () {
        this.initmap();
    };
    map.prototype.start = function () {
    };
    map.prototype.initmap = function () {
        /*
        //根据图层名获取图层
        let layer:cc.TiledLayer = this.map.getLayer("Layer1");
        //获取图层的行列数
        let layerSize:cc.Size = layer.getLayerSize();
        let width = layerSize.width;
        let height = layerSize.height;
        console.log(layerSize);  // size(width:40, height:30)
        //获取图层的gid
        let mapData = [];
        for(let i=0;i<height;i++){
            mapData[i] = [];
            for(let j=0;j<width;j++){
                mapData[i].push(layer.getTileGIDAt(new cc.Vec2(j,i)));
            }
        }
        console.log(mapData);*/
        var tilemap = this.node.getComponent(cc.TiledMap);
        var layer = tilemap.getLayer("barrier");
        var tilesize = tilemap.getTileSize();
        var layersize = layer.getLayerSize();
        /*let mapData = [];
        for(var i=0 ; i<layersize.height;i++){
            mapData[i] = [];
            for(var j=0;j<layersize.width;j++){
                mapData[i].push(layer.getTileGIDAt(new cc.Vec2(j,i)));
            }
        }
        console.log(mapData);*/
        for (var i = 0; i < layersize.width; i++) {
            for (var j = 0; j < layersize.height; j++) {
                var tile = layer.getTiledTileAt(i, j, true);
                if (tile.gid == 130) {
                    var body = tile.node.addComponent(cc.RigidBody);
                    body.type = cc.RigidBodyType.Static;
                    var collider = tile.node.addComponent(cc.PhysicsBoxCollider);
                    collider.offset = cc.v2(tilesize.width / 2, tilesize.height / 2);
                    collider.size = tilesize;
                    collider.tag = 1;
                    collider.apply();
                }
            }
        }
    };
    map = __decorate([
        ccclass
    ], map);
    return map;
}(cc.Component));
exports.default = map;

cc._RF.pop();