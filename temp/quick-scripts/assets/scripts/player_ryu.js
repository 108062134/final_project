(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/player_ryu.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '7701aRAEe9IQ5x4/M/UZ64O', 'player_ryu', __filename);
// scripts/player_ryu.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player_ryu = /** @class */ (function (_super) {
    __extends(Player_ryu, _super);
    function Player_ryu() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.anim = null; //this will use to get animation component
        _this.animateState = null; //this will use to record animationState
        _this.bulletPrefab = null;
        _this.bulletPool = null; // this is a bullet manager, and it control the bullet resource
        _this.playerxSpeed = 0;
        _this.playerySpeed = 0;
        _this.aDown = false; // key for player to go left
        _this.dDown = false; // key for player to go right
        _this.eDown = false; // key for player to shoot
        // private spaceDown: boolean = false; // key for player to jump
        _this.wDown = false;
        _this.sDown = false;
        _this.qDown = false;
        _this.combo = 0;
        _this.health = 100;
        _this.blocked = false;
        _this.object_picked = null;
        _this.isDead = false;
        return _this;
    }
    Player_ryu.prototype.onLoad = function () {
        this.anim = this.getComponent(cc.Animation);
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        this.bulletPool = new cc.NodePool('Bullet_ryu');
        var maxBulletNum = 5;
        for (var i = 0; i < maxBulletNum; i++) {
            var bullet = cc.instantiate(this.bulletPrefab);
            this.bulletPool.put(bullet);
            cc.log("bulletpull");
        }
    };
    Player_ryu.prototype.start = function () {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.anim.on("finished", this.onAnimFinished, this);
    };
    Player_ryu.prototype.update = function (dt) {
        this.playerMovement(dt);
        this.playerAnimation();
    };
    Player_ryu.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        }
        else if (other.node.group == "object") {
            other.node.group = "player";
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);
        }
    };
    Player_ryu.prototype.onEndContact = function (contact, self, other) {
        this.blocked = false;
    };
    Player_ryu.prototype.onKeyDown = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                this.aDown = true;
                this.dDown = false;
                break;
            case cc.macro.KEY.d:
                this.dDown = true;
                this.aDown = false;
                break;
            case cc.macro.KEY.e:
                this.eDown = true;
                break;
            case cc.macro.KEY.w:
                this.wDown = true;
                this.sDown = false;
                break;
            case cc.macro.KEY.s:
                this.sDown = true;
                this.wDown = false;
                break;
            case cc.macro.KEY.q:
                this.qDown = true;
                break;
        }
    };
    Player_ryu.prototype.onKeyUp = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.w:
                this.wDown = false;
                break;
            case cc.macro.KEY.s:
                this.sDown = false;
                break;
            case cc.macro.KEY.e:
                this.eDown = false;
                break;
            case cc.macro.KEY.a:
                this.aDown = false;
                break;
            case cc.macro.KEY.d:
                this.dDown = false;
                break;
            case cc.macro.KEY.q:
                this.qDown = false;
                break;
        }
    };
    Player_ryu.prototype.onAnimFinished = function (e, finishingAnim) {
        if (finishingAnim.name == "lefthand_ryu" || finishingAnim.name == "righthand_ryu") {
            this.combo = (this.combo + 1) % 2;
            //this.state = this.stateList.notAttacking;
        }
    };
    Player_ryu.prototype.playerMovement = function (dt) {
        if (this.isDead) {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if (this.eDown || this.anim.getAnimationState('hadouken').isPlaying) {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if (this.aDown) {
            this.playerxSpeed = -100;
        }
        else if (this.dDown) {
            this.playerxSpeed = 100;
        }
        else if (this.wDown) {
            this.playerySpeed = 100;
        }
        else if (this.sDown) {
            this.playerySpeed = -100;
        }
        else {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        this.node.x += this.playerxSpeed * dt; //move player
        this.node.y += this.playerySpeed * dt;
    };
    Player_ryu.prototype.playerAnimation = function () {
        this.node.scaleX = (this.aDown) ? -1 : (this.dDown) ? 1 : this.node.scaleX;
        /*if(this.isDead)
        {
            //reset player position and play reborn animation
            if(this.animateState == null || this.animateState.name != 'reborn')
            {
                this.node.setPosition(cc.v2(102,-16));
                this.animateState = this.anim.play('reborn');
                this.anim.once("finished",() =>{
                    this.isDead = false;
                })
                
            }
        }*/
        if (!this.anim.getAnimationState('hadouken').isPlaying) // move animation can play only when shoot or jump animation finished
         {
            if (this.eDown)
                this.animateState = this.anim.play('hadouken');
            else if (this.aDown || this.dDown || this.wDown || this.sDown) {
                if (this.animateState == null || this.animateState.name != 'walk') // when first call or last animation is not move
                    this.animateState = this.anim.play('walk');
            }
            else if (this.qDown) {
                if (this.combo == 0) {
                    if (this.animateState == null || this.animateState.name != "lefthand_ryu") {
                        this.animateState = this.anim.play("lefthand_ryu");
                    }
                }
                else if (this.combo == 1) {
                    if (this.animateState == null || this.animateState.name != "righthand_ryu") {
                        this.animateState = this.anim.play("righthand_ryu");
                    }
                }
            }
            else {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if (this.animateState == null || this.animateState.name != 'idle_ryu')
                    this.animateState = this.anim.play('idle_ryu');
            }
        }
    };
    Player_ryu.prototype.createBullet = function () {
        var bullet = null;
        if (this.bulletPool.size() > 0)
            bullet = this.bulletPool.get(this.bulletPool);
        if (bullet != null)
            bullet.getComponent('Bullet_ryu').init(this.node);
        cc.log("yes bullet");
    };
    __decorate([
        property(cc.Prefab)
    ], Player_ryu.prototype, "bulletPrefab", void 0);
    Player_ryu = __decorate([
        ccclass
    ], Player_ryu);
    return Player_ryu;
}(cc.Component));
exports.default = Player_ryu;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=player_ryu.js.map
        