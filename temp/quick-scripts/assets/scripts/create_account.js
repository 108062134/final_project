(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/create_account.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '50979yL0BlATZaAAXY0XwJr', 'create_account', __filename);
// scripts/create_account.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var create_account = /** @class */ (function (_super) {
    __extends(create_account, _super);
    function create_account() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    create_account.prototype.onLoad = function () {
        var button = new cc.Component.EventHandler();
        button.target = this.node;
        button.component = "create_account";
        button.handler = "create";
        button.customEventData = "";
        cc.find("enter").getComponent(cc.Button).clickEvents.push(button);
        var back = new cc.Component.EventHandler();
        back.target = this.node;
        back.component = "create_account";
        back.handler = "back";
        back.customEventData = "";
        cc.find("back").getComponent(cc.Button).clickEvents.push(back);
    };
    create_account.prototype.back = function () {
        cc.director.loadScene("login");
    };
    create_account.prototype.create = function () {
        var email = cc.find("email").getComponent(cc.EditBox).string;
        var password = cc.find("password").getComponent(cc.EditBox).string;
        var id = cc.find("id").getComponent(cc.EditBox).string;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function (result) {
            email = "";
            password = "";
            var Ref = firebase.database().ref(id + '/record');
            var data = {
                best_money: 0,
                fuck: true,
            };
            Ref.set(data);
            console.log("success");
            cc.director.loadScene("login");
        }).catch(function (error) {
            email = "";
            password = "";
            cc.log("wrong");
        });
    };
    create_account.prototype.start = function () {
    };
    create_account = __decorate([
        ccclass
    ], create_account);
    return create_account;
}(cc.Component));
exports.default = create_account;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=create_account.js.map
        