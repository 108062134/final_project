const { ccclass, property } = cc._decorator;
import Player from "./player"

@ccclass
export default class NewClass extends cc.Component {

    type: string = "";

    speed: number = 0;

    dir: number = 1;

    onCollide: boolean = false;

    @property(Player)
    player: Player = null;

    @property(cc.SpriteFrame)
    deadSprite = null;

    @property(cc.Prefab)
    turtleShell = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        this.node.name = "spear";
        this.node.group = "enemy";
        this.type = "enemy";
        this.speed = 80;
    }

    update(dt) {
        if(this.node.x - this.player.node.x < 160 && this.node.x - this.player.node.x > 0){
            this.node.x -= this.speed * dt;
        }
        else if(this.player.node.x - this.node.x < 160 && this.player.node.x - this.node.x > 0){
            this.node.x += this.speed * dt;
        }
        if(this.node.y - this.player.node.y < 160 && this.node.y - this.player.node.y > 0){
            this.node.y -= this.speed * dt;
        }
        else if(this.player.node.y - this.node.y < 160 && this.player.node.y - this.node.y > 0){
            this.node.y += this.speed * dt;
        }
        if (this.onCollide) this.onCollide = false;
    }

    deleteEntity() {
        this.getComponent(cc.Sprite).spriteFrame = this.deadSprite;
        this.getComponent(cc.RigidBody).enabled = false;
        setTimeout(() => {
            this.node.destroy();
        }, 500);
    }

    onBeginContact(contact, selfCollider, otherCollider) {
        if (otherCollider.tag == 1) { // terrain
            if (this.onCollide == false) {
                this.dir *= -1;
                this.node.scaleX *= -1;
            }
            this.onCollide = true;
        }
        if (otherCollider.node.name == "snake") {
            this.deleteEntity();
        }
    }
}
