// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class menu extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        let Menu = new cc.Component.EventHandler();
        Menu.target = this.node;
        Menu.component ="menu";
        Menu.handler = "menu_onclick";
        Menu.customEventData = "";
        cc.find("menu").getComponent(cc.Button).clickEvents.push(Menu);

        let alone = new cc.Component.EventHandler();
        alone.target = this.node;
        alone.component ="menu";
        alone.handler = "alone_onclick";
        alone.customEventData = "";
        cc.find("singlePlayerMode").getComponent(cc.Button).clickEvents.push(alone);

        let multiplayer = new cc.Component.EventHandler();
        multiplayer.target = this.node;
        multiplayer.component ="menu";
        multiplayer.handler = "multi_onclick";
        multiplayer.customEventData = "";
        cc.find("multi_player_mode").getComponent(cc.Button).clickEvents.push(multiplayer);
    }
    alone_onclick(){
        cc.director.loadScene("singlePlayerMode");
    }
    multi_onclick(){
        cc.director.loadScene("multi_player_mode");
    }
    menu_onclick(){
        cc.director.loadScene("scoreboard");
    }
    start () {

    }

    // update (dt) {}
}
                            