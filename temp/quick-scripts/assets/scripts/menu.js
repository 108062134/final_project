(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/menu.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'fd77aaLtqNLMLNrPQlQhvbN', 'menu', __filename);
// scripts/menu.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var menu = /** @class */ (function (_super) {
    __extends(menu, _super);
    function menu() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // LIFE-CYCLE CALLBACKS:
    menu.prototype.onLoad = function () {
        var Menu = new cc.Component.EventHandler();
        Menu.target = this.node;
        Menu.component = "menu";
        Menu.handler = "menu_onclick";
        Menu.customEventData = "";
        cc.find("menu").getComponent(cc.Button).clickEvents.push(Menu);
        var alone = new cc.Component.EventHandler();
        alone.target = this.node;
        alone.component = "menu";
        alone.handler = "alone_onclick";
        alone.customEventData = "";
        cc.find("singlePlayerMode").getComponent(cc.Button).clickEvents.push(alone);
        var multiplayer = new cc.Component.EventHandler();
        multiplayer.target = this.node;
        multiplayer.component = "menu";
        multiplayer.handler = "multi_onclick";
        multiplayer.customEventData = "";
        cc.find("multi_player_mode").getComponent(cc.Button).clickEvents.push(multiplayer);
    };
    menu.prototype.alone_onclick = function () {
        cc.director.loadScene("singlePlayerMode");
    };
    menu.prototype.multi_onclick = function () {
        cc.director.loadScene("multi_player_mode");
    };
    menu.prototype.menu_onclick = function () {
        cc.director.loadScene("scoreboard");
    };
    menu.prototype.start = function () {
    };
    menu = __decorate([
        ccclass
    ], menu);
    return menu;
}(cc.Component));
exports.default = menu;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=menu.js.map
        