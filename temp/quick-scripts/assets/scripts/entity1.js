(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/entity1.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, 'dc4b4lBq0xLypZpnK+GzkQN', 'entity1', __filename);
// scripts/entity1.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var player_1 = require("./player");
var NewClass = /** @class */ (function (_super) {
    __extends(NewClass, _super);
    function NewClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.linear_velocity = new cc.Vec2(100, 100);
        _this.player = null;
        _this.onCollide = false;
        _this.state = 0;
        //speed: number = 100;
        _this.type = "";
        _this.health = 100;
        _this.anim = null;
        _this.animState = null;
        _this.isDead = false;
        _this.isAattack01Completed = false;
        _this.combo = 0;
        _this.standAnim = null;
        _this.standAnimState = null;
        _this.blocked = false;
        _this.dir = 1;
        _this.object_picked = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {}
    NewClass.prototype.start = function () {
        this.node.name = "goblin";
        this.node.group = "enemy";
        this.type = "enemy";
        this.anim = this.getComponent(cc.Animation);
    };
    NewClass.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        }
        else if (other.node.group == "object") {
            other.node.group = "player";
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);
        }
    };
    NewClass.prototype.onEndContact = function (contact, self, other) {
        this.blocked = false;
    };
    NewClass.prototype.update = function (dt) {
        this.playerMovement(dt);
        this.playerAnimation();
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
    };
    NewClass.prototype.playerMovement = function (dt) {
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
        if (this.node.x - this.player.node.x < 300 && this.node.x - this.player.node.x > 50) {
            this.speed.x = -25;
            this.node.scaleX = 1;
        }
        else if (this.player.node.x - this.node.x < 300 && this.player.node.x - this.node.x > 50) {
            this.speed.x = 25;
            this.node.scaleX = -1;
        }
        else if (this.player.node.x - this.node.x > 300) {
        }
        else if (this.node.x - this.player.node.x > 300) {
        }
        if (this.node.y - this.player.node.y < 300 && this.node.y - this.player.node.y > 50) {
            this.speed.y = -25;
        }
        else if (this.player.node.y - this.node.y < 300 && this.player.node.y - this.node.y > 50) {
            this.speed.y = 25;
        }
        else if (this.player.node.y - this.node.y > 300) {
        }
        else if (this.node.y - this.player.node.y > 300) {
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = this.speed;
        console.log(this.player.node.x);
        console.log(this.player.node.y);
        console.log(this.node.x);
        console.log(this.node.y);
    };
    NewClass.prototype.playerAnimation = function () {
        if (this.isDead) {
            this.animState = this.anim.play("death");
        }
        if (this.player.node.x - this.node.x < 50 && this.player.node.x - this.node.x > 0 && ((this.player.node.y - this.node.y < 50 && this.player.node.y - this.node.y > 0) || this.node.y - this.player.node.y < 50 && this.node.y - this.player.node.y > 0)) {
            if (!this.anim.getAnimationState("attack").isPlaying) {
                this.animState = this.anim.play("attack");
            }
        }
        else if (this.node.x - this.player.node.x < 50 && this.node.x - this.player.node.x > 0 && ((this.player.node.y - this.node.y < 50 && this.player.node.y - this.node.y > 0) || this.node.y - this.player.node.y < 50 && this.node.y - this.player.node.y > 0)) {
            if (!this.anim.getAnimationState("attack").isPlaying) {
                this.animState = this.anim.play("attack");
            }
        }
        else if (this.node.x - this.player.node.x < 200 && this.node.x - this.player.node.x > 50) {
            if (!this.anim.getAnimationState("move").isPlaying) {
                this.animState = this.anim.play("move");
            }
        }
        else if (this.player.node.x - this.node.x < 200 && this.player.node.x - this.node.x > 50) {
            if (!this.anim.getAnimationState("move").isPlaying) {
                this.animState = this.anim.play("move");
            }
        }
        else if (this.player.node.x - this.node.x > 200) {
            if (!this.anim.getAnimationState("stand").isPlaying) {
                this.animState = this.anim.play("stand");
            }
        }
        else if (this.node.x - this.player.node.x > 200) {
            if (!this.anim.getAnimationState("stand").isPlaying) {
                this.animState = this.anim.play("stand");
            }
        }
        if (this.node.y - this.player.node.y < 200 && this.node.y - this.player.node.y > 50) {
            if (!this.anim.getAnimationState("move").isPlaying) {
                this.animState = this.anim.play("move");
            }
        }
        else if (this.player.node.y - this.node.y < 200 && this.player.node.y - this.node.y > 50) {
            if (!this.anim.getAnimationState("move").isPlaying) {
                this.animState = this.anim.play("move");
            }
        }
        else if (this.player.node.y - this.node.y > 200) {
            if (!this.anim.getAnimationState("stand").isPlaying) {
                this.animState = this.anim.play("stand");
            }
        }
        else if (this.node.y - this.player.node.y > 200) {
            if (!this.anim.getAnimationState("stand").isPlaying) {
                this.animState = this.anim.play("stand");
            }
        }
    };
    __decorate([
        property(cc.Vec2)
    ], NewClass.prototype, "linear_velocity", void 0);
    __decorate([
        property(player_1.default)
    ], NewClass.prototype, "player", void 0);
    NewClass = __decorate([
        ccclass
    ], NewClass);
    return NewClass;
}(cc.Component));
exports.default = NewClass;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=entity1.js.map
        