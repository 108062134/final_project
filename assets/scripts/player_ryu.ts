// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player_ryu extends cc.Component {

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;


    private bulletPool = null; // this is a bullet manager, and it control the bullet resource

    private playerxSpeed: number = 0;

    private playerySpeed: number = 0;

    private aDown: boolean = false; // key for player to go left

    private dDown: boolean = false; // key for player to go right

    private eDown: boolean = false; // key for player to shoot

   // private spaceDown: boolean = false; // key for player to jump

    private wDown:boolean = false;

    private sDown:boolean = false;

    private qDown:boolean = false;

    private combo:number = 0;

    health: number = 100;

    blocked = false;

    private object_picked = null;

    private isDead: boolean = false;

    onLoad()
    {
        
        this.anim=this.getComponent(cc.Animation);
        cc.director.getCollisionManager().enabled = true;

        cc.director.getPhysicsManager().enabled = true;

        this.bulletPool = new cc.NodePool('Bullet_ryu');

        let maxBulletNum = 5;

        for(let i: number = 0; i < maxBulletNum; i++)
        {
            let bullet = cc.instantiate(this.bulletPrefab);

            this.bulletPool.put(bullet);
            cc.log("bulletpull");
        }
    }

    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);

        this.anim.on("finished", this.onAnimFinished, this);
    }

    update(dt)
    {
        this.playerMovement(dt);

        this.playerAnimation();
    }

    onBeginContact(contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        } else if (other.node.group == "object") {
            other.node.group = "player"
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);

        }
    }
    onEndContact(contact, self, other) {
        this.blocked = false;
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.a:

                this.aDown = true;

                this.dDown = false;

                break;

            case cc.macro.KEY.d:

                this.dDown = true;

                this.aDown = false;

                break;


            case cc.macro.KEY.e:

                this.eDown = true;

                break;
            case cc.macro.KEY.w:

                this.wDown = true;

                this.sDown = false;

                break;
            case cc.macro.KEY.s:
                
                this.sDown = true;

                this.wDown = false;

                break;
            case cc.macro.KEY.q:

                this.qDown = true;
    
                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.w:

                this.wDown = false;

                break;

            case cc.macro.KEY.s:

                this.sDown = false;

                break;


            case cc.macro.KEY.e:

                this.eDown = false;

                break;
            case cc.macro.KEY.a:

                this.aDown = false;

                break;
            case cc.macro.KEY.d:
                
                this.dDown = false;

                break;
            case cc.macro.KEY.q:

                this.qDown = false;

                break;
        }
    }

    onAnimFinished(e, finishingAnim) {
        if (finishingAnim.name == "lefthand_ryu" || finishingAnim.name == "righthand_ryu") {
            this.combo = (this.combo + 1) % 2;
            //this.state = this.stateList.notAttacking;
        }
    }

    private playerMovement(dt)
    {
        if(this.isDead){
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if(this.eDown || this.anim.getAnimationState('hadouken').isPlaying){
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if(this.aDown){
            this.playerxSpeed = -100;
        } 
        else if(this.dDown){
            this.playerxSpeed = 100;
        }
        else if(this.wDown){
            this.playerySpeed = 100;
        }
        else if(this.sDown){
            this.playerySpeed = -100;
        }
        else{
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        this.node.x += this.playerxSpeed * dt;  //move player
        this.node.y += this.playerySpeed * dt;
    }

    private playerAnimation()
    {
        this.node.scaleX = (this.aDown) ? -1 : (this.dDown) ? 1 : this.node.scaleX;

        /*if(this.isDead)
        {
            //reset player position and play reborn animation
            if(this.animateState == null || this.animateState.name != 'reborn')
            {
                this.node.setPosition(cc.v2(102,-16));
                this.animateState = this.anim.play('reborn');
                this.anim.once("finished",() =>{
                    this.isDead = false;
                })
                
            }
        }*/
        if(!this.anim.getAnimationState('hadouken').isPlaying) // move animation can play only when shoot or jump animation finished
        {
            if(this.eDown)
                this.animateState = this.anim.play('hadouken');
            else if(this.aDown || this.dDown || this.wDown || this.sDown)
            {
                if(this.animateState == null || this.animateState.name != 'walk') // when first call or last animation is not move
                    this.animateState = this.anim.play('walk');
            }
            else if(this.qDown)
            {
                if (this.combo == 0) {
                    if (this.animateState == null || this.animateState.name != "lefthand_ryu") {
                        this.animateState = this.anim.play("lefthand_ryu");
                    }
                }
                else if (this.combo == 1) {
                    if (this.animateState == null || this.animateState.name != "righthand_ryu") {
                        this.animateState = this.anim.play("righthand_ryu");
                    }
                }
            }
            else
            {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if(this.animateState == null || this.animateState.name != 'idle_ryu')
                    this.animateState = this.anim.play('idle_ryu');
            }
        }
    }

    private createBullet()
    {
        let bullet = null;

        if (this.bulletPool.size() > 0) 
            bullet = this.bulletPool.get(this.bulletPool);

        if(bullet != null)
            bullet.getComponent('Bullet_ryu').init(this.node);
        cc.log("yes bullet");
    }


}
