// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;
import zawarudo from "./zawarudo";

@ccclass
export default class player extends cc.Component {

    @property(zawarudo)
    zawarudo: zawarudo = null;

    @property(cc.Vec2)
    private linear_velocity: cc.Vec2 = new cc.Vec2(100, 100);

    @property(cc.Prefab)
    explosionEffect: cc.Prefab = null;

    state: number = 0;
    stateList = {
        notAttacking: 0,
        attacking: 1,
        usingUltimate: 2
    }
    input = {};
    //speed: number = 100;
    speed: cc.Vec2;
    health: number = 100;
    anim = null;
    animState = null;
    isDead: boolean = false;
    isAattack01Completed: boolean = false;
    combo: number = 0;
    standAnim = null;
    standAnimState = null;
    blocked = false;
    private object_picked = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.object_picked = cc.sequence(cc.moveBy(0.15, 0, 40), cc.moveBy(0.3, 0, -20));
    }

    start() {
        console.log("start");
        cc.systemEvent.on("keydown", this.onKeyDown, this);
        cc.systemEvent.on("keyup", this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
        this.anim.on("finished", this.onAnimFinished, this);
        this.standAnim = this.zawarudo.getComponent(cc.Animation);
        this.standAnim.on("finished", this.onStandAnimFinished, this.zawarudo);
    }
    onBeginContact(contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        } else if (other.node.group == "object") {
            other.node.group = "player"
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);

        }
    }
    onEndContact(contact, self, other) {
        this.blocked = false;
    }

    update(dt) {
        this.playerMovement(dt);
        this.playerAnimation();
        this.standAnimation();
    }

    onDestroy() {
        cc.systemEvent.off("keydown", this.onKeyDown, this);
        cc.systemEvent.off("keyup", this.onKeyUp, this);
    }

    onKeyDown(e) {
        this.input[e.keyCode] = 1;
    }

    onKeyUp(e) {
        this.input[e.keyCode] = 0;
        if (e.keyCode == cc.macro.KEY.w) {
            this.zawarudo.node.active = !this.zawarudo.node.active;
        }
    }

    onAnimFinished(e, finishingAnim) {
        if (finishingAnim.name == "attack01" || finishingAnim.name == "attack02") {
            console.log(this.state);
            this.combo = (this.combo + 1) % 2;
            this.state = this.stateList.notAttacking;
        }
    }

    onStandAnimFinished(e, finishingAnim) {
        if (finishingAnim.name == "zawarudo_attack") {
            console.log(this.state);
            this.state = this.stateList.notAttacking;
        }
    }

    playerMovement(dt) {
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
        /*if (this.input[cc.macro.KEY.shift] == 1) {
            this.speed = 200;
        }
        else {
            this.speed = 100;
        }*/
        if (this.input[cc.macro.KEY.left] == 1) {
            //this.node.x -= this.speed * dt;
            this.speed.y = 0;
            this.speed.x = -100;
        }
        else if (this.input[cc.macro.KEY.right] == 1) {
            //this.node.x += this.speed * dt;
            this.speed.y = 0;
            this.speed.x = 100;
        }
        else if (this.input[cc.macro.KEY.up] == 1) {
            //this.node.y += this.speed * dt;
            this.speed.y = 100;
            this.speed.x = 0;
        }
        else if (this.input[cc.macro.KEY.down] == 1) {
            //this.node.y -= this.speed * dt;
            this.speed.y = -100;
            this.speed.x = 0;
        }
        else {
            this.speed.x = this.speed.y = 0;
        }
        if (this.input[cc.macro.KEY.shift]) {
            this.speed.y *= 2;
            this.speed.x *= 2;
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = this.speed;
    }

    playerAnimation() {
        this.node.scaleX = this.input[cc.macro.KEY.left] ? -1 : this.input[cc.macro.KEY.right] ? 1 : this.node.scaleX;
        // the direction which player is facing
        if (this.isDead) {

        }
        // only play moving animations when player isn't attacking
        if (this.input[cc.macro.KEY.r]) {
            if (this.state != this.stateList.attacking) {
                this.ultimate();
            }
        }
        if (this.input[cc.macro.KEY.q] == 1) {
            if (this.state != this.stateList.attacking && this.state != this.stateList.usingUltimate) {
                if (this.zawarudo.node.active == false) {
                    this.state = this.stateList.attacking;
                    if (this.combo == 0) {
                        if (this.animState == null || this.animState.name != "attack01") {
                            this.animState = this.anim.play("attack01");
                        }
                    }
                    else if (this.combo == 1) {
                        if (this.animState == null || this.animState.name != "attack02") {
                            this.animState = this.anim.play("attack02");
                        }
                    }
                }
            }
        }
        if (this.state == this.stateList.notAttacking) {
            if (this.input[cc.macro.KEY.up] ||
                this.input[cc.macro.KEY.left] ||
                this.input[cc.macro.KEY.down] ||
                this.input[cc.macro.KEY.right]) {
                if (this.input[cc.macro.KEY.shift] == 1) {
                    if (this.animState == null || this.animState.name != "run") {
                        this.animState = this.anim.play("run");
                    }
                }
                else if (this.animState == null || this.animState.name != "walk") {
                    this.animState = this.anim.play("walk");
                }
            }
            else {
                if (this.animState == null || this.animState.name != "idle") {
                    this.animState = this.anim.play("idle");
                }
            }
        }
    }

    standAnimation() {
        if (this.input[cc.macro.KEY.q]) {
            if (this.zawarudo.state != this.zawarudo.stateList.attacking) {
                if (this.standAnimState == null || this.standAnimState.name != "zawarudo_attack")
                    this.standAnimState = this.standAnim.play("zawarudo_attack");
                this.zawarudo.state = this.zawarudo.stateList.attacking;
            }
        }
        if (this.zawarudo.state == this.zawarudo.stateList.notAttacking) {
            if (this.standAnimState == null || this.standAnimState.name != "zawarudo_idle")
                this.standAnimState = this.standAnim.play("zawarudo_idle");
        }
    }

    ultimate() {
        if (this.state != this.stateList.usingUltimate) {
            this.state = this.stateList.usingUltimate;
            this.getComponent(cc.RigidBody).active = false;
            this.node.runAction(cc.sequence(
                cc.callFunc(() => { this.animState = this.anim.play("beforeUlt") }),
                cc.delayTime(this.animState.duration + 1),
                cc.callFunc(() => { this.animState = this.anim.play("jump") }),
                cc.moveBy(1, 0, cc.find("Canvas").height / 2),
                cc.callFunc(() => {
                    this.animState = this.anim.play("carryRoadroller");
                    this.node.anchorY = 0;
                }),
                cc.moveBy(1, 0, -(cc.find("Canvas").height) / 2),
                cc.callFunc(() => { this.animState = this.anim.play("roadrollerLanding"); }),
                cc.delayTime(this.animState.duration + 1),
                cc.callFunc(() => { this.animState = this.anim.play("punchRoadroller"); }),
                cc.delayTime(3),
                cc.callFunc(() => {
                    let explosion = cc.instantiate(this.explosionEffect);
                    this.node.addChild(explosion);
                    explosion.setPosition(0, 0);
                    explosion.getComponent(cc.Animation).play("explosion");
                    cc.delayTime(explosion.getComponent(cc.Animation).getAnimationState("explosion").duration);
                    setTimeout(() => {
                        explosion.destroy();
                    }, explosion.getComponent(cc.Animation).getAnimationState("explosion").duration)
                }),


                cc.callFunc(() => {
                    this.state = this.stateList.notAttacking
                    this.node.anchorY = 0.5;
                })
            ));
        }
    }
}
