(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/player.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '8fde4JkElZCQYIMfpsXwsZ6', 'player', __filename);
// scripts/player.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var zawarudo_1 = require("./zawarudo");
var player = /** @class */ (function (_super) {
    __extends(player, _super);
    function player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.zawarudo = null;
        _this.linear_velocity = new cc.Vec2(100, 100);
        _this.explosionEffect = null;
        _this.state = 0;
        _this.stateList = {
            notAttacking: 0,
            attacking: 1,
            usingUltimate: 2
        };
        _this.input = {};
        _this.health = 100;
        _this.anim = null;
        _this.animState = null;
        _this.isDead = false;
        _this.isAattack01Completed = false;
        _this.combo = 0;
        _this.standAnim = null;
        _this.standAnimState = null;
        _this.blocked = false;
        _this.object_picked = null;
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    player.prototype.onLoad = function () {
        this.object_picked = cc.sequence(cc.moveBy(0.15, 0, 40), cc.moveBy(0.3, 0, -20));
    };
    player.prototype.start = function () {
        console.log("start");
        cc.systemEvent.on("keydown", this.onKeyDown, this);
        cc.systemEvent.on("keyup", this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
        this.anim.on("finished", this.onAnimFinished, this);
        this.standAnim = this.zawarudo.getComponent(cc.Animation);
        this.standAnim.on("finished", this.onStandAnimFinished, this.zawarudo);
    };
    player.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        }
        else if (other.node.group == "object") {
            other.node.group = "player";
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);
        }
    };
    player.prototype.onEndContact = function (contact, self, other) {
        this.blocked = false;
    };
    player.prototype.update = function (dt) {
        this.playerMovement(dt);
        this.playerAnimation();
        this.standAnimation();
    };
    player.prototype.onDestroy = function () {
        cc.systemEvent.off("keydown", this.onKeyDown, this);
        cc.systemEvent.off("keyup", this.onKeyUp, this);
    };
    player.prototype.onKeyDown = function (e) {
        this.input[e.keyCode] = 1;
    };
    player.prototype.onKeyUp = function (e) {
        this.input[e.keyCode] = 0;
        if (e.keyCode == cc.macro.KEY.w) {
            this.zawarudo.node.active = !this.zawarudo.node.active;
        }
    };
    player.prototype.onAnimFinished = function (e, finishingAnim) {
        if (finishingAnim.name == "attack01" || finishingAnim.name == "attack02") {
            console.log(this.state);
            this.combo = (this.combo + 1) % 2;
            this.state = this.stateList.notAttacking;
        }
    };
    player.prototype.onStandAnimFinished = function (e, finishingAnim) {
        if (finishingAnim.name == "zawarudo_attack") {
            console.log(this.state);
            this.state = this.stateList.notAttacking;
        }
    };
    player.prototype.playerMovement = function (dt) {
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
        /*if (this.input[cc.macro.KEY.shift] == 1) {
            this.speed = 200;
        }
        else {
            this.speed = 100;
        }*/
        if (this.input[cc.macro.KEY.left] == 1) {
            //this.node.x -= this.speed * dt;
            this.speed.y = 0;
            this.speed.x = -100;
        }
        else if (this.input[cc.macro.KEY.right] == 1) {
            //this.node.x += this.speed * dt;
            this.speed.y = 0;
            this.speed.x = 100;
        }
        else if (this.input[cc.macro.KEY.up] == 1) {
            //this.node.y += this.speed * dt;
            this.speed.y = 100;
            this.speed.x = 0;
        }
        else if (this.input[cc.macro.KEY.down] == 1) {
            //this.node.y -= this.speed * dt;
            this.speed.y = -100;
            this.speed.x = 0;
        }
        else {
            this.speed.x = this.speed.y = 0;
        }
        if (this.input[cc.macro.KEY.shift]) {
            this.speed.y *= 2;
            this.speed.x *= 2;
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = this.speed;
    };
    player.prototype.playerAnimation = function () {
        this.node.scaleX = this.input[cc.macro.KEY.left] ? -1 : this.input[cc.macro.KEY.right] ? 1 : this.node.scaleX;
        // the direction which player is facing
        if (this.isDead) {
        }
        // only play moving animations when player isn't attacking
        if (this.input[cc.macro.KEY.r]) {
            if (this.state != this.stateList.attacking) {
                this.ultimate();
            }
        }
        if (this.input[cc.macro.KEY.q] == 1) {
            if (this.state != this.stateList.attacking && this.state != this.stateList.usingUltimate) {
                if (this.zawarudo.node.active == false) {
                    this.state = this.stateList.attacking;
                    if (this.combo == 0) {
                        if (this.animState == null || this.animState.name != "attack01") {
                            this.animState = this.anim.play("attack01");
                        }
                    }
                    else if (this.combo == 1) {
                        if (this.animState == null || this.animState.name != "attack02") {
                            this.animState = this.anim.play("attack02");
                        }
                    }
                }
            }
        }
        if (this.state == this.stateList.notAttacking) {
            if (this.input[cc.macro.KEY.up] ||
                this.input[cc.macro.KEY.left] ||
                this.input[cc.macro.KEY.down] ||
                this.input[cc.macro.KEY.right]) {
                if (this.input[cc.macro.KEY.shift] == 1) {
                    if (this.animState == null || this.animState.name != "run") {
                        this.animState = this.anim.play("run");
                    }
                }
                else if (this.animState == null || this.animState.name != "walk") {
                    this.animState = this.anim.play("walk");
                }
            }
            else {
                if (this.animState == null || this.animState.name != "idle") {
                    this.animState = this.anim.play("idle");
                }
            }
        }
    };
    player.prototype.standAnimation = function () {
        if (this.input[cc.macro.KEY.q]) {
            if (this.zawarudo.state != this.zawarudo.stateList.attacking) {
                if (this.standAnimState == null || this.standAnimState.name != "zawarudo_attack")
                    this.standAnimState = this.standAnim.play("zawarudo_attack");
                this.zawarudo.state = this.zawarudo.stateList.attacking;
            }
        }
        if (this.zawarudo.state == this.zawarudo.stateList.notAttacking) {
            if (this.standAnimState == null || this.standAnimState.name != "zawarudo_idle")
                this.standAnimState = this.standAnim.play("zawarudo_idle");
        }
    };
    player.prototype.ultimate = function () {
        var _this = this;
        if (this.state != this.stateList.usingUltimate) {
            this.state = this.stateList.usingUltimate;
            this.getComponent(cc.RigidBody).active = false;
            this.node.runAction(cc.sequence(cc.callFunc(function () { _this.animState = _this.anim.play("beforeUlt"); }), cc.delayTime(this.animState.duration + 1), cc.callFunc(function () { _this.animState = _this.anim.play("jump"); }), cc.moveBy(1, 0, cc.find("Canvas").height / 2), cc.callFunc(function () {
                _this.animState = _this.anim.play("carryRoadroller");
                _this.node.anchorY = 0;
            }), cc.moveBy(1, 0, -(cc.find("Canvas").height) / 2), cc.callFunc(function () { _this.animState = _this.anim.play("roadrollerLanding"); }), cc.delayTime(this.animState.duration + 1), cc.callFunc(function () { _this.animState = _this.anim.play("punchRoadroller"); }), cc.delayTime(3), cc.callFunc(function () {
                var explosion = cc.instantiate(_this.explosionEffect);
                _this.node.addChild(explosion);
                explosion.setPosition(0, 0);
                explosion.getComponent(cc.Animation).play("explosion");
                cc.delayTime(explosion.getComponent(cc.Animation).getAnimationState("explosion").duration);
                setTimeout(function () {
                    explosion.destroy();
                }, explosion.getComponent(cc.Animation).getAnimationState("explosion").duration);
            }), cc.callFunc(function () {
                _this.state = _this.stateList.notAttacking;
                _this.node.anchorY = 0.5;
            })));
        }
    };
    __decorate([
        property(zawarudo_1.default)
    ], player.prototype, "zawarudo", void 0);
    __decorate([
        property(cc.Vec2)
    ], player.prototype, "linear_velocity", void 0);
    __decorate([
        property(cc.Prefab)
    ], player.prototype, "explosionEffect", void 0);
    player = __decorate([
        ccclass
    ], player);
    return player;
}(cc.Component));
exports.default = player;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=player.js.map
        