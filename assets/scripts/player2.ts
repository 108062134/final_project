// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class player2 extends cc.Component {

    @property(cc.Vec2)
    private linear_velocity: cc.Vec2 = new cc.Vec2(100, 100);

    state: number = 0;
    stateList = {
        notAttacking: 0,
        attacking: 1
    }
    input = {};
    //speed: number = 100;
    speed: cc.Vec2;
    health: number = 100;
    anim = null;
    animState = null;
    isDead: boolean = false;
    isAattack01Completed: boolean = false;
    combo: number = 0;
    standAnim = null;
    standAnimState = null;
    blocked = false;
    private object_picked = null;
    private hit_interval = 0.6;
    private can_hit = true;
    private can_smite = true;
    private can_spin = true;
   
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.object_picked = cc.sequence(cc.moveBy(0.15, 0, 40), cc.moveBy(0.3, 0, -20));
    }

    start() {
        console.log("start");
        cc.systemEvent.on("keydown", this.onKeyDown, this);
        cc.systemEvent.on("keyup", this.onKeyUp, this);
        this.anim = this.getComponent(cc.Animation);
    }
    onBeginContact(contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        } else if (other.node.group == "object") {
            other.node.group = "player"
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);

        }
    }
    onEndContact(contact, self, other) {
        this.blocked = false;
    }

    update(dt) {
        this.playerMovement(dt);
        this.playerAnimation();
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;
    }

    onDestroy() {
        cc.systemEvent.off("keydown", this.onKeyDown, this);
        cc.systemEvent.off("keyup", this.onKeyUp, this);
    }

    onKeyDown(e) {
        this.input[e.keyCode] = 1;
    }

    onKeyUp(e) {
        this.input[e.keyCode] = 0;
    }



    playerMovement(dt) {
        this.speed = this.node.getComponent(cc.RigidBody).linearVelocity;

        if (this.input[cc.macro.KEY.left] == 1) {
            //this.node.x -= this.speed * dt;
            this.speed.y = 0;
            this.speed.x = -100;
        }
        else if (this.input[cc.macro.KEY.right] == 1) {
            //this.node.x += this.speed * dt;
            this.speed.y = 0;
            this.speed.x = 100;
        }
        else if (this.input[cc.macro.KEY.up] == 1) {
            //this.node.y += this.speed * dt;
            this.speed.y = 100;
            this.speed.x = 0;
        }
        else if (this.input[cc.macro.KEY.down] == 1) {
            //this.node.y -= this.speed * dt;
            this.speed.y = -100;
            this.speed.x = 0;
        } else {
            this.speed.x = this.speed.y = 0;
        }
        this.node.getComponent(cc.RigidBody).linearVelocity = this.speed;
    }

    playerAnimation() {
        this.node.scaleX = this.input[cc.macro.KEY.left] ? -1 : this.input[cc.macro.KEY.right] ? 1 : this.node.scaleX;
        // the direction which player is facing
        if (this.isDead) {

        }
        // only play moving animations when player isn't attacking
        
        if(this.input[cc.macro.KEY.e]){
            if(!this.anim.getAnimationState("spin").isPlaying && this.can_spin){
                this.animState = this.anim.play("spin");
                this.can_spin = false;
                this.scheduleOnce(function(){
                    this.can_spin = true;
                },this.hit_interval);
            }
        }
        else if(this.input[cc.macro.KEY.w]){
            if(!this.anim.getAnimationState("smite").isPlaying && this.can_smite){
                this.animState = this.anim.play("smite");
                this.can_smite = false;
                this.scheduleOnce(function(){
                    this.can_smite = true;
                },this.hit_interval);
            }
        }else if(this.input[cc.macro.KEY.q]){
            
            if(!this.anim.getAnimationState("hit").isPlaying && this.can_hit){
                this.animState = this.anim.play("hit");
                this.can_hit = false;
                this.scheduleOnce(function(){
                    this.can_hit = true;
                },this.hit_interval);
            }
        }else if (this.input[cc.macro.KEY.up] ||
            this.input[cc.macro.KEY.left] ||
            this.input[cc.macro.KEY.down] ||
            this.input[cc.macro.KEY.right]) {

            if(!this.anim.getAnimationState("run").isPlaying){
                this.animState = this.anim.play("run");
            }
        }
        if( (!this.input[cc.macro.KEY.up] &&
            !this.input[cc.macro.KEY.left] &&
            !this.input[cc.macro.KEY.down] &&
            !this.input[cc.macro.KEY.right]) && this.anim.getAnimationState("run").isPlaying){
                this.anim.play("idle");
        }


    }


}
