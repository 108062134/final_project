(function() {"use strict";var __module = CC_EDITOR ? module : {exports:{}};var __filename = 'preview-scripts/assets/scripts/player_rockman.js';var __require = CC_EDITOR ? function (request) {return cc.require(request, require);} : function (request) {return cc.require(request, __filename);};function __define (exports, require, module) {"use strict";
cc._RF.push(module, '5bae9N5jAlCzoC9RwarntyA', 'player_rockman', __filename);
// scripts/player_rockman.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Player = /** @class */ (function (_super) {
    __extends(Player, _super);
    function Player() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.anim = null; //this will use to get animation component
        _this.animateState = null; //this will use to record animationState
        _this.bulletPrefab = null;
        _this.bulletPool = null; // this is a bullet manager, and it control the bullet resource
        _this.playerxSpeed = 0;
        _this.playerySpeed = 0;
        _this.zDown = false; // key for player to go left
        _this.xDown = false; // key for player to go right
        _this.jDown = false; // key for player to shoot
        _this.kDown = false; // key for player to jump
        _this.cDown = false;
        _this.vDown = false;
        _this.health = 100;
        _this.blocked = false;
        _this.object_picked = null;
        _this.isDead = true;
        return _this;
        //check if the collision is valid or not
    }
    Player.prototype.onLoad = function () {
        this.anim = this.getComponent(cc.Animation);
        cc.director.getCollisionManager().enabled = true;
        cc.director.getPhysicsManager().enabled = true;
        this.bulletPool = new cc.NodePool('Bullet');
        var maxBulletNum = 5;
        for (var i = 0; i < maxBulletNum; i++) {
            var bullet = cc.instantiate(this.bulletPrefab);
            this.bulletPool.put(bullet);
        }
    };
    Player.prototype.start = function () {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    };
    Player.prototype.update = function (dt) {
        this.playerMovement(dt);
        this.playerAnimation();
    };
    Player.prototype.onBeginContact = function (contact, self, other) {
        if (other.node.group == "barrier") {
            this.blocked = true;
        }
        else if (other.node.group == "object") {
            other.node.group = "player";
            other.node.runAction(this.object_picked);
            this.scheduleOnce(function () {
                other.node.active = false;
            }, 0.5);
        }
    };
    Player.prototype.onEndContact = function (contact, self, other) {
        this.blocked = false;
    };
    Player.prototype.onKeyDown = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.z:
                this.zDown = true;
                this.xDown = false;
                break;
            case cc.macro.KEY.x:
                this.xDown = true;
                this.zDown = false;
                break;
            case cc.macro.KEY.j:
                this.jDown = true;
                break;
            case cc.macro.KEY.k:
                this.kDown = true;
                break;
            case cc.macro.KEY.c:
                this.cDown = true;
                this.vDown = false;
                break;
            case cc.macro.KEY.v:
                this.vDown = true;
                this.cDown = false;
                break;
        }
    };
    Player.prototype.onKeyUp = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.z:
                this.zDown = false;
                break;
            case cc.macro.KEY.x:
                this.xDown = false;
                break;
            case cc.macro.KEY.j:
                this.jDown = false;
                break;
            case cc.macro.KEY.k:
                this.kDown = false;
                break;
            case cc.macro.KEY.c:
                this.cDown = false;
                break;
            case cc.macro.KEY.v:
                this.vDown = false;
                break;
        }
    };
    Player.prototype.playerMovement = function (dt) {
        if (this.isDead) {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if (this.jDown || this.anim.getAnimationState('shoot').isPlaying) {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        else if (this.zDown)
            this.playerxSpeed = -300;
        else if (this.xDown)
            this.playerxSpeed = 300;
        else if (this.cDown)
            this.playerySpeed = 300;
        else if (this.vDown)
            this, this.playerySpeed = -300;
        else {
            this.playerxSpeed = 0;
            this.playerySpeed = 0;
        }
        this.node.x += this.playerxSpeed * dt; //move player
        this.node.y += this.playerySpeed * dt;
    };
    Player.prototype.playerAnimation = function () {
        var _this = this;
        this.node.scaleX = (this.zDown) ? -1 : (this.xDown) ? 1 : this.node.scaleX;
        if (this.isDead) {
            //reset player position and play reborn animation
            if (this.animateState == null || this.animateState.name != 'reborn') {
                this.node.setPosition(cc.v2(102, -16));
                this.animateState = this.anim.play('reborn');
                this.anim.once("finished", function () {
                    _this.isDead = false;
                });
            }
        }
        else if (!this.anim.getAnimationState('shoot').isPlaying && !this.anim.getAnimationState('jump').isPlaying) // move animation can play only when shoot or jump animation finished
         {
            if (this.jDown)
                this.animateState = this.anim.play('shoot');
            else if (this.kDown) {
                this.animateState = this.anim.play('jump');
            }
            else if (this.zDown || this.xDown || this.cDown || this.vDown) {
                if (this.animateState == null || this.animateState.name != 'move') // when first call or last animation is not move
                    this.animateState = this.anim.play('move');
            }
            else {
                //if no key is pressed and the player is on ground, stop all animations and go back to idle
                if (this.animateState == null || this.animateState.name != 'idle')
                    this.animateState = this.anim.play('idle');
            }
        }
    };
    //give velocity to the player
    // call this when player shoots the bullet.
    Player.prototype.createBullet = function () {
        var bullet = null;
        if (this.bulletPool.size() > 0)
            bullet = this.bulletPool.get(this.bulletPool);
        if (bullet != null)
            bullet.getComponent('Bullet').init(this.node);
        cc.log("bullet rock");
    };
    __decorate([
        property(cc.Prefab)
    ], Player.prototype, "bulletPrefab", void 0);
    Player = __decorate([
        ccclass
    ], Player);
    return Player;
}(cc.Component));
exports.default = Player;

cc._RF.pop();
        }
        if (CC_EDITOR) {
            __define(__module.exports, __require, __module);
        }
        else {
            cc.registerModuleFunc(__filename, function () {
                __define(__module.exports, __require, __module);
            });
        }
        })();
        //# sourceMappingURL=player_rockman.js.map
        